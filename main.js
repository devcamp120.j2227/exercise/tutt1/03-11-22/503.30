class Car {
    constructor(name,year) {
        this.name= name;
        this.year= year;
    }
    print() {
        console.log(this.name + "-" + this.year)
    }
    age(currentYear) {
        return currentYear - this.year
    }
}
let car1 = new Car("Meserari", 2022);
car1.print();
console.log(car1.age(2024));;

class Car2 extends Car {
    constructor(name,year,country) {
        super(name,year);
        this.country = country
    }
    print2() {
        console.log("country "+ this.country);
    }
}

let car2 = new Car2("Meserari", 2022,"Việt Nam");
car2.print();
car2.print2();